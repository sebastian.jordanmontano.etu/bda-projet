# NoSQL Databases: When is it good to chose a NoSQL database technology

Sebastian JORDAN MONTAÑO

M2 - Génie Logiciel

***

## Introduction

SQL databases are shown to be highly efficient. They are widely used in the world. With the arriving of Big Data and distributed systems, they started to shown their downsides as they were not conceived to address those types of problems. Nowadays, the world of software evolves specially fast and the databases need to keep track of this evolution too. In the context of Big Data, this is specially true as the data also changes continuously its shape. As SQL databases have a rigid schema, making them to keep track of those changes is a complex task.
NonSQL databases are flexible in that sense. They schema facilitates the evolution of the database of its schema as it is not rigid. Also, as they are newer technologies, most of the NoSQL databases were conceived to address problems related to Distributed Systems. They have also shown to be more efficient than SQL databases for that purpose. This project is meant to do a literature review of the work for evaluating the performance of NoSQL databases to better understand their limitations and advantages and to know in which situations they can be used the best.

## Performance Evaluation of NoSQL Databases in Distributed Systems

Kleit et al. [1] did a performance analysis of three NoSQL database technologies: MongoDB, Cassandra and Riak. For the authors, performance is the number of read and write operations that can be executed per second. Their client has a distributed system that stores the Electronic Health Record (EHR) data of patients. The need to store the data of nine million patients in servers that are located in different locations in the world. The data must be store for 99 years and it grows at over one terabyte per month.
This client, wanted to develop a new part of the system using NoSQL databases instead of SQL. As they need to keep the data for a long time and the data shape may change over the time, it is logical to consider  a NoSQL approach due to the flexibility of the schema.

They did the benchmarks of the databases on a nine-node system using that they deployed using AWS. The nine-node configuration used a topology that represented the geographically distributed production system.
The authors identified that a typical workflow of the EHR system (Electronic Health Record) consists of 80% read and 20% write operations. So, the operations used for the tets had the 80% read and 20% write operations rate.

The results obtained vary from 3200 to 225 operations per second. Cassandra being the one with the highest rate and MongoDB the one with the lowest one. For the latency, Cassandra was the one with the highest latency. MongoDB was 5 to 4 times faster in terms of latency, being the fastest of the three.

## NoSQL Databases as a Solution for BigData

The SQL databases dominated the market for years and they are still the default choice in most of the situations when a database is needed for a software system. They are proved to be efficient. Nevertheless, with the arriving of big data and distributed system, the needs changed and relational databases do not adapt properly to them.

Venkatraman et al. [2] made an literature review for making a comparison between SQL versus NoSQL databases in the context of Big Data analytics for business situations. They grouped the non-relational databases into 4 groups of data models: Key-Value Store databases; Column Oriented databases; Document Store databases; and Graph Store databases.

They identified the following as the current major challenges that Big Data presents to businesses:

- High data Velocity – rapidly and continuously updated data streams from different sources and locations.
- Data Variety – structured, semi-structured and unstructured data storage.
- Data Volume – huge number of datasets with sizes of several terabytes or petabytes.
- Data Complexity – data organized in several different locations or data centres.

After reviewing the related work of the context of non-relational databases and the performance analysis of NoSQL versus SQL, the authors concluded that the NoSQL databases are better fit for overcoming the challenges of Big Data. They also present interesting results as Choi et al. [3] that found that MongoDB, a Document Store NoSQL database, stored posts 850% faster than a SQL database. Taking into account the CAP theorem for distributed systems, the improvement of stability and performance is at the expense of data consistency or availability. This is also the case for SQL distributed databases.

NoSQL databases, as they have flexible schemas, they are better to evolve along with to the data that changes in shape constantly.

## Limitations of NoSQL Databases

It is important to note that NoSQL databases also have limitations. Venkatraman et al. [2] identified the following as possible constraints:

- NoSQL is new and immature; therefore, there is lack of familiarity and limited expertise.
- NoSQL databases scale horizontally by giving up either consistency or availability.
- There is no standard query and manipulation language in all NoSQL databases.
- There is no standard interface for NoSQL databases SQL Versus NoSQL Movement with Big Data Analytics 63
- It is difficult to export all data in distributed ones (Cassandra) compared to non-distributed ones (MongoDB).
- NoSQL databases are challenging to install and difficult to maintain.

## Evolving an SQL Schema

As discussed before, software systems constantly evolve. To adapt to those changes, the databases that come along with those software system need to evolve as well. Evolving an SQL database can be a complex task and it is an ongoing research topic.

De Jong et at. [5] made an analysis of the state of the art of the existing approaches for migrating an SQL schema. These approaches take into account that many times in production systems, one wants to have the old and the new versions running in parallel. So, it is necessary that the SQL database is compatible with both versions of the system. The propose a new open source approach: `QuantumDB`.

Wu et al. [4] made an analysis of the schema evolution of four popular open source programs that use embedded databases: Firefox, Monotone, BiblioteQ and Vienna. They show that their SQL databases suffer alterations of the schema constantly. For example, they show that Firefox made 29 changes to the SQL database schema per year in average.

## Conclusion

SQL databases, also called relational, dominate the world of databases. They are a strong technology that proved its efficiency. They were conceived before the distributed system existed. So, they are not meant to address the problems that distributed systems have.
We have seen that NoSQL, or no-relational databases have many advantages for use cases of the modern world. Talking about Big Data and distributed systems, there are NoSQL solutions that were conceived for solving those problems and they perform pretty well, better than SQL solutions. We also saw that, thanks to the schemaless design of NoSQL databases, evolving them is simpler and feasible than migrating a rigid SQL one.

Both SQL and NoSQL solutions they have limitations. NoSQL will not replace the existence of SQL databases, by the contrary, an optimal scenario is to them to coexist to complement each other addressing each others limitations.

## Bibliography

1. Klein J. et al. (2015). Performance Evaluation of NoSQL Databases: A Case Study. Proceedings of the 1st Workshop on Performance Analysis of Big Data Systems.
2. Venkatraman S. et al. (2016). SQL Versus NoSQL Movement with Big Data Analytics. International Journal of Information Technology and Computer Science.
3. Choi, Y., Jeon, W., & Yo, S. (2014), 'Improving Database System Performance by Applying NoSQL', Journal Of Information Processing Systems, 10(3), 355-364.
4. S. Wu and I. Neamtiu, "Schema evolution analysis for embedded databases," 2011 IEEE 27th International Conference on Data Engineering Workshops, 2011.
5. Jong, M.D., & Deursen, A.V. (2015). Continuous deployment and schema evolution in SQL databases.